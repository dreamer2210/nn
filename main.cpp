#include <iostream>
#include "NeuNet.h"
#include <chrono>

using namespace std;

// Тестирование

int main(int argc, char* argv[])
{
    NeuNet neural("neural_OR.txt");

    for(size_t i = 0; true; ++i)
    {
        neural.AddNeuron(2, act_sigmoid);

        neural.DelNeuron(2);
    }

    system("pause");

    return 0;
}