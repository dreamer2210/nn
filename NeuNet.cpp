#include "NeuNet.h"

#define WEIGHT_CONST (double(rand() % 1000) / 1000 - 0.5)

template<typename T>
matrix operator * (const std::vector<T>& a, const std::vector<Neuron>& b)
{
    matrix result;

    result.Init(a.size(), b.size());

    for(size_t i = 0; i < a.size(); ++i)
    {
        for(size_t j = 0; j < b.size(); ++j)
        {
            result[i][j] = a[i] * b[j].dataOut;
        }
    }

    return result;
}

void act_sigmoid(double& x)
{
    x = 1/(1 + exp(-x));
}

void act_tanh(double& x)
{
    x = (2/(1+exp(-2 * x))) - 1;
}

void act_relu(double& x)
{
    if(x < 0)
        x = 0;
}

// ������������

NeuNet::NeuNet(int countLayers, std::vector<int>& countNeurons)
{
    Weights = new matrix[countLayers - 1];
    Neurons.resize(countLayers);

    for(size_t i = 0; i < countLayers; ++i)
    {
        Neurons[i].resize(countNeurons[i]);
    }
}

NeuNet::NeuNet(std::string fileName)
{
    std::ifstream istream;
    unsigned int rows, cols;
    double temp;

    istream.open(fileName);

    istream >> temp;

    this->Neurons.resize(temp);

    Weights = new matrix[Neurons.size() - 1];

    for(size_t i = 0; i < Neurons.size() - 1; ++i)
    {
        istream >> rows;
        istream >> cols;

        Weights[i].Init(rows, cols);

        istream >> Weights[i];

        if(i % 2 == 0)
        {
            Neurons[i].resize(cols);
            Neurons[i + 1].resize(rows);
        }
    }

    Neurons[Neurons.size() - 1].resize(rows);
}

// ������ 

int NeuNet::AddNeuron(unsigned int iLayer, void(*act)(double& x))
{
    if(iLayer < 0 || iLayer >= Neurons.size())
        return 1;
    else if(act == 0)
        return 2;

    Neurons[iLayer].resize(Neurons[iLayer].size() + 1);
    Neurons[iLayer][Neurons[iLayer].size() - 1].ptrActivate = act;

    if(int(iLayer - 1) >= 0)
    {
        Weights[iLayer - 1].mat.resize(Weights[iLayer - 1].mat.size() + 1);

        Weights[iLayer - 1].mat[Weights[iLayer - 1].mat.size() - 1].resize(Neurons[iLayer].size());

        for(size_t i = 0; i < Neurons[iLayer].size(); ++i)
        {
            Weights[iLayer - 1].mat[i][Neurons[iLayer].size() - 1] = WEIGHT_CONST;
        }
    }
    if(iLayer + 1 < Neurons.size())
    {
        for(size_t i = 0; i < Neurons[iLayer + 1].size(); ++i)
        {
            Weights[iLayer].mat[i].resize(Weights[iLayer].mat[i].size() + 1);

            Weights[iLayer].mat[i][Weights[iLayer].mat[i].size() - 1] = WEIGHT_CONST;
        }
    }

    return 0;
}

int NeuNet::DelNeuron(unsigned int iLayer)
{
    if(iLayer < 0 || iLayer >= Neurons.size())
        return 1;
    else if(Neurons[iLayer].size() < 2)
        return 2;

    Neurons[iLayer].resize(Neurons[iLayer].size() - 1);

    if(int(iLayer - 1) >= 0)
    {
        Weights[iLayer - 1].mat.resize(Weights[iLayer - 1].mat.size() - 1);
    }
    if(iLayer + 1 < Neurons.size())
    {
        for(size_t i = 0; i < Neurons[iLayer + 1].size(); ++i)
        {
            Weights[iLayer].mat[i].resize(Weights[iLayer].mat[i].size() - 1);
        }
    }

    return 0;    
}

int NeuNet::SaveNet(std::string nameFile)
{
    std::ofstream ostream;

    ostream.open(nameFile);

    if(!ostream)
        return -1;

    ostream << Neurons.size() << std::endl;

    for(size_t i = 0; i < Neurons.size() - 1; ++i)
    {
        ostream << Weights[i] << std::endl;
    }

    return 0;
}

int NeuNet::GetNet(std::string nameFile)
{
    std::ifstream istream;
    unsigned int rows, cols;
    double temp;

    istream.open(nameFile);

    if(!istream)
        return -1;

    if(this->Neurons.size() != 0)
        delete[] Weights;

    istream >> temp;

    this->Neurons.resize(temp);

    Weights = new matrix[Neurons.size() - 1];

    for(size_t i = 0; i < Neurons.size() - 1; ++i)
    {
        istream >> rows;
        istream >> cols;

        Weights[i].Init(rows, cols);

        istream >> Weights[i];
    }

    return 0;
}

void NeuNet::Generate()
{
    srand(time(0));

    for(size_t i = 0; i < Neurons.size() - 1; ++i)
    {
        Weights[i].Init(Neurons[i + 1].size(), Neurons[i].size());

        for(size_t j = 0; j < Neurons[i + 1].size(); ++j)
        {
            for(size_t k = 0; k < Neurons[i].size(); ++k)
            {
                Weights[i][j][k] = WEIGHT_CONST;
            }
        }
    }
}

void NeuNet::QueryNeuronThread(unsigned int layer, unsigned int rows)
{
    double temp = 0;

    for(size_t i = 0; i < Neurons[layer].size(); ++i)
    {
        temp += Weights[layer][rows][i] * Neurons[layer][i].dataOut;
    }
            
    Neurons[layer + 1][rows].dataOut = temp;
    Neurons[layer + 1][rows].ptrActivate(Neurons[layer + 1][rows].dataOut);
}

std::vector<double> NeuNet::Query(const std::vector<double>& dataIn)
{
    std::vector<double> result(Neurons[Neurons.size() - 1].size());
    double temp = 0;

    for(size_t i = 0; i < dataIn.size(); ++i)
        Neurons[0][i].dataOut = dataIn[i];

    for(size_t i = 0; i < Neurons.size() - 1; ++i)
    {
        for(size_t j = 0; j < Neurons[i + 1].size(); ++j)
        {
            for(size_t k = 0; k < Neurons[i].size(); ++k)
            {
                temp += Weights[i][j][k] * Neurons[i][k].dataOut;
            }
            
            Neurons[i + 1][j].dataOut = temp;
            Neurons[i + 1][j].ptrActivate(Neurons[i + 1][j].dataOut);

            temp = 0;
        }
    }

    for(size_t i = 0; i < result.size(); ++i)
        result[i] = Neurons[Neurons.size() - 1][i];

    return result;
}

std::vector<double> NeuNet::QueryM(const std::vector<double>& dataIn)
{
    unsigned int numCore = std::thread::hardware_concurrency();

    std::vector<double> result(Neurons[Neurons.size() - 1].size());
    double temp = 0;

    std::thread cores[numCore];

    for(size_t i = 0; i < dataIn.size(); ++i)
        Neurons[0][i].dataOut = dataIn[i];

    for(unsigned int i = 0; i < Neurons.size() - 1; ++i)
    {
        for(unsigned int j = 0, k; j < Neurons[i + 1].size(); j += k)
        {
            for(k = 0; k < numCore && j + k < Neurons[i + 1].size(); ++k)
            {
                cores[k] = std::thread(QueryNeuronThread, this, i, j + k);
            }

            for(k = 0; k < numCore && j + k < Neurons[i + 1].size(); ++k)
            {
                cores[k].join();
            }
        }
    }

    for(size_t i = 0; i < result.size(); ++i)
        result[i] = Neurons[Neurons.size() - 1][i];

    return result;
}

int NeuNet::BackProp(double rateLearn, double errorMin, const std::vector<double>& dataIn, const std::vector<double>& dataOut)
{
    // ���-�� ���������� �����, ��� ������� ������ �� ��������
    size_t countErrors = 0;
    // ��� errorExit <= errorMin ����� ��������� ������
    double errorExit = 100;
    // ���������� ���������� �������� errorExit
    double errorExitBefore;
    // ����� ��� ���������� ������������
    std::vector<double> temp(5);
    // ������� ����� �������� � �������� ����������� ������ Query()
    std::vector<double> error(Neurons[Neurons.size() - 1].size());
    // ��������� ������ Query()
    std::vector<double> result(Neurons[Neurons.size() - 1].size());
    // ������� ������ ���� ����� ���������
    matrix deltaNeurons;

    deltaNeurons.mat.resize(Neurons.size());

    for(size_t i = 0; i < Neurons.size(); ++i)
    {
        deltaNeurons[i].resize(Neurons[i].size());
    }

    while(errorExit > errorMin)
    {
        result = QueryM(dataIn);

        // ������� ����� �������� � ���������� ������������.
        for(size_t i = 0; i < error.size(); ++i)
        {
            error[i] = (dataOut[i] - result[i]);
                    
            errorExitBefore = errorExit;

            if(error[i] < errorExit)
            {
                errorExit = error[i];
            }

            if(errorExit == errorExitBefore)
                ++countErrors;
        }

        // ���� ������ �� ���������� ����� n ���������� �����, ���������� -1
        if(countErrors == 100000)
            return -1;

        deltaNeurons[Neurons.size() - 1] = error;

        for(long int i = Neurons.size() - 2; i >= 0; --i)
        {
            // �������� ��������������� ������ �� ����� ���������
            deltaNeurons[i] = Weights[i].Transform() * deltaNeurons[i + 1];

            for(size_t j = 0; j < Neurons[i].size(); ++j)
            {
                for(size_t k = 0; k < Neurons[i + 1].size(); ++k)
                {
                    temp.resize(Neurons[i + 1].size());

                    for(size_t o = 0; o < Neurons[i + 1].size(); ++o)
                    {
                        temp[o] = deltaNeurons[i + 1][o] * Neurons[i + 1][o].dataOut * (1 - Neurons[i + 1][o].dataOut); 
                    }
                }
            }

            // ���������� ������� �����
            Weights[i] = Weights[i] + temp * Neurons[i] * rateLearn;
        }
    }

    return 0;
}

int  NeuNet::BackProp(double rateLearn, double errorMin, const matrix& dataIn, const matrix& dataOut, unsigned int countData)
{
    // ���-�� ���������� �����, ��� ������� ������ �� ��������
    size_t countErrors = 0;
    // ��� errorExit <= errorMin ����� ��������� ������
    double errorExit = 0;
    // ���������� ���������� �������� errorExit
    double errorExitBefore;
    // ����� ��� ���������� ������������
    std::vector<double> temp(5);
    // ������� ����� �������� � �������� ����������� ������ Query()
    std::vector<double> error(Neurons[Neurons.size() - 1].size());
    // ��������� ������ Query()
    std::vector<double> result(Neurons[Neurons.size() - 1].size());
    // ������� ������ ���� ����� ���������
    matrix deltaNeurons;

    deltaNeurons.mat.resize(Neurons.size());

    for(size_t i = 0; i < Neurons.size(); ++i)
    {
        deltaNeurons[i].resize(Neurons[i].size());
    }

    do
    {
        for(size_t n = 0; n < countData; ++n)
        {
            result = QueryM(dataIn[n]);

            // ������� ����� �������� � ���������� ������������.
            for(size_t i = 0; i < error.size(); ++i)
            {
                error[i] = (dataOut[n][i] - result[i]);
                    
                errorExitBefore = errorExit;

                if(error[i] < errorExit)
                {
                    errorExit = abs(error[i]);
                }

                if(errorExit == errorExitBefore)
                    ++countErrors;
                else
                    countErrors = 0;
            }

            // ���� ������ �� ���������� ����� n ���������� �����, ���������� -1
            if(countErrors == 1000000)
                return -1;

            deltaNeurons[Neurons.size() - 1] = error;

            for(long int i = Neurons.size() - 2; i >= 0; --i)
            {
                // �������� ��������������� ������ �� ����� ���������
                deltaNeurons[i] = Weights[i].Transform() * deltaNeurons[i + 1];

                for(size_t j = 0; j < Neurons[i].size(); ++j)
                {
                    for(size_t k = 0; k < Neurons[i + 1].size(); ++k)
                    {
                        temp.resize(Neurons[i + 1].size());

                        for(size_t o = 0; o < Neurons[i + 1].size(); ++o)
                        {
                            temp[o] = deltaNeurons[i + 1][o] * Neurons[i + 1][o] * (1 - Neurons[i + 1][o]); 
                        }
                    }
                }

                // ���������� ������� �����
                Weights[i] = Weights[i] + temp * Neurons[i] * rateLearn;
            }
        }
    }
    while(errorExit > errorMin);

    return 0;
}

