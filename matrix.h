#pragma once
#include <vector>
#include <initializer_list>
#include <ostream>
#include <istream>

// ����� ������� ��� ������ ���������


class matrix
{
public:
    std::vector<std::vector<double>> mat;

// ������������

    // ����������� ����������� ��� �������������� ������ ��� ����������� ��������
    matrix(const matrix& obj);

    // ������� �����������
    matrix(const std::vector<std::vector<double>>& obj);

    // ���� ���������� ���������� �������, �� �������� �������
    matrix(unsigned int rows, unsigned int cols);

    // ������ �����������. (���� ����������� ���� ����� >>)
    matrix(){};

// ������

    // ������ ������� ������ � ������� ������������ ���������
    matrix Transform();

    // ��-�� ���������� mat �������� �������� ���� �����
    matrix& Init(unsigned int rows, unsigned int cols);

// ������������� ��������� ��� ������ � ��������

    matrix operator * (const matrix& obj) const;
    matrix operator * (double a) const;
    matrix operator + (const matrix& obj) const;
    matrix operator - (const matrix& obj) const;

    matrix& operator = (const matrix& obj);

// ������������� ��������� ��� ������ � ��������� �������

    friend std::vector<double> operator * (const matrix& a, const std::vector<double>& b);

// ������������� ��������� ��� ������� � ������� mat

    std::vector<double>& operator [] (int i);
    const std::vector<double>& operator [] (int i) const;

// ������������� ��������� �����/������ �������

    // ������� ������� �����������, ����� ����������
    friend std::ostream& operator << (std::ostream& stream, const matrix& obj);

    // ������� ������ �����������, ����� ����������
    friend std::istream& operator >> (std::istream& stream, matrix& obj);
};