#include "matrix.h"

// ������������

    matrix::matrix(const matrix& obj)
    {
        this->mat.resize(obj.mat.size());

        for(size_t i = 0; i < obj.mat.size(); ++i)
        {
            this->mat[i].resize(obj.mat[i].size());

            for(size_t j = 0; j < obj.mat.size(); ++j)
            {
                this->mat[i][j] = obj[i][j];
            }
        }
    }

    matrix::matrix(const std::vector<std::vector<double>>& obj)
    {
        this->mat.resize(obj.size());

        for (size_t i = 0; i < obj.size(); ++i)
        {
            this->mat[i].resize(obj[i].size());

            for(size_t j = 0; j < obj[i].size(); ++j)
            {
                this->mat[i][j] = obj[i][j];
            }
        }
        
    }

    matrix::matrix(unsigned int rows, unsigned int cols)
    {
        this->mat.resize(rows);

        for(size_t i = 0; i < rows; ++i)
        {
            this->mat[i].resize(cols);
        }
    }

// ������

    matrix matrix::Transform()
    {
        matrix result(this->mat[0].size(), this->mat.size());

        for(size_t i = 0; i < this->mat.size(); ++i)
        {
            for(size_t j = 0; j < this->mat[i].size(); ++j)
            {
                result.mat[j][i] = mat[i][j];
            }
        }

        return result;
    }

    matrix& matrix::Init(unsigned int rows, unsigned int cols)
    {
        mat.resize(rows);

        for(size_t i = 0; i < rows; ++i)
        {
            mat[i].resize(cols);
        }

        return *this;
    }
    
// ������������� ��������� �����/������ �������

    std::ostream& operator << (std::ostream& stream, const matrix& obj)
    {
        stream << obj.mat.size() << '\t' << obj.mat[0].size() << std::endl;;

        for(size_t i = 0; i < obj.mat.size(); ++i)
        {
            for(size_t j = 0; j < obj.mat[i].size(); ++j)
            {
                stream << obj[i][j] << '\t';
            }

            stream << std::endl;
        }

        return stream;
    }
    
    std::istream& operator >> (std::istream& stream, matrix& obj)
    {
        for(size_t i = 0; i < obj.mat.size(); ++i)
        {
            for(size_t j = 0; j < obj.mat[i].size(); ++j)
            {
                stream >> obj.mat[i][j];
            }
        }

        return stream;
    }

// ������������� ��������� ��� ������� � ������� mat

    std::vector<double>& matrix::operator [] (int i)
    {
        return mat[i];
    }

    const std::vector<double>& matrix::operator [] (int i) const
    {
        return mat[i];
    }

// ������������� ��������� ��� ������ � ��������

    matrix matrix::operator * (const matrix& obj) const
    {
        matrix result(this->mat.size(), obj.mat[0].size());

        for(size_t i = 0; i < this->mat.size(); ++i)
        {
            for(size_t j = 0; j < obj.mat[0].size(); ++j)
            {
                for(size_t k = 0; k < this->mat[0].size(); ++k)
                {
                    result[i][j] += this->mat[i][k] * obj.mat[k][j]; 
                }
            }
        }

        return result;
    }

    matrix matrix::operator * (double a) const
    {
        matrix result(this->mat.size(), this->mat[0].size());

        for(size_t i = 0; i < this->mat.size(); ++i)
        {
            for(size_t j = 0; j < this->mat[i].size(); ++j)
            {
                result[i][j] = this->mat[i][j] * a;
            }
        }

        return result;
    }

    matrix matrix::operator + (const matrix& obj) const
    {
        matrix result(this->mat.size(), this->mat[0].size());

        for(size_t i = 0; i < this->mat.size(); ++i)
        {
            for(size_t j = 0; j < this->mat[0].size(); ++j)
            {
                result[i][j] = this->mat[i][j] + obj[i][j];
            }
        }

        return result;
    }

    matrix matrix::operator - (const matrix& obj) const
    {
        matrix result(this->mat.size(), this->mat[0].size());

        for(size_t i = 0; i < obj.mat[0].size(); ++i)
        {
            for(size_t j = 0; j < this->mat[0].size(); ++j)
            {
                result[i][j] = this->mat[i][j] - obj[i][j];
            }
        }

        return result;
    }

    matrix& matrix::operator = (const matrix& obj)
    {
        this->mat.resize(obj.mat.size());

        for(size_t i = 0; i < mat.size(); ++i)
        {
            this->mat[i].resize(obj.mat[i].size());

            for(size_t j = 0; j < mat[i].size(); ++j)
            {
                this->mat[i][j] = obj.mat[i][j];
            }
        }

        return *this;
    }

// ������������� ��������� ��� ������ � ��������� �������

    std::vector<double> operator * (const matrix& a, const std::vector<double>& b)
    {
        std::vector<double> result(a.mat.size());

        for(size_t i = 0; i < a.mat.size(); ++i)
        {
            for(size_t j = 0; j < b.size(); ++j)
            {
                result[i] += a[i][j] * b[j]; 
            }
        }

        return result;
    }

