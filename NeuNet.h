#pragma once
#include "matrix.h"
#include <thread>
#include <random>
#include <math.h>
#include <ctime>
#include <fstream>
#include <string>

struct Neuron;

// �������� ������������ �������� � ������� a[n][1] * b[1][m] = c[n][m] (��� ���������� ����� �����)
template<typename T>
matrix operator * (const std::vector<T>& a, const std::vector<Neuron>& b);

// ������� ���������
void act_sigmoid(double& x);

void act_tanh(double& x);

void act_relu(double& x);

struct Neuron
{
    double dataOut;
    
    // ����� � ������� ������� ���� ���� ������� ���������, ������� ������������ � Query()
    void (*ptrActivate)(double& x);

    operator double() { return dataOut; }
};

// ����� ���������

class NeuNet
{
    // ������� �����. 
    // i - ����; j - ������ ���������� ����; k - ������ ����������� ���� (��-�� ����������� ������������ ������)
    matrix* Weights;

    // ������� ��������
    std::vector<std::vector<Neuron>> Neurons;

    void QueryNeuronThread(unsigned int layer, unsigned int rows);
public:

// ������������

    NeuNet(int countLayers, std::vector<int>& countNeurons);
    NeuNet(std::string fileName);
    NeuNet()
    {
        
    }
    ~NeuNet()
    {
        delete[] Weights;
    }
// ������
    
    int AddNeuron(unsigned int layer, void(*act)(double& x));
    int DelNeuron(unsigned int layer);

    // ���������� ������� �����; 
    // ������: ����� ������� ����� ����� 0 � 1 ������
    // GetWeight(0);
    matrix& GetWeight(unsigned int i) { return Weights[i]; }

    std::vector<std::vector<Neuron>>& GetNeuron() { return Neurons; }

    // ��������� ������ ��������� � ����. 
    int SaveNet(std::string nameFile);

    // �������� ������ ��������� �� ����� � ��������� � �������
    int GetNet(std::string nameFile);

    // ����� ��������� �������� ����� � �������� (-1;+1)
    void Generate();

    // ����� ������ ��������� (������ �� ���������)
    std::vector<double> Query(const std::vector<double>& dataIn);

    // ������������� ����� ������ ��������� (������ �� ���������)
    std::vector<double> QueryM(const std::vector<double>& dataIn);

    // ���������� � ����� ������� ������
    int BackProp(double rateLearn, double errorMin, const std::vector<double>& dataIn, const std::vector<double>& dataOut);

    // ���������� � ����������� �������� ������
    int BackProp(double rateLearn, double errorMin, const matrix& dataIn, const matrix& dataOut, unsigned int countData);
};